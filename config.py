from flask import Flask, request, render_template
from flask import render_template
from elasticsearch import Elasticsearch 
import os


list_json = [ {'id':10, 'dir_name':'xx'},
              {'id':20, 'dir_name':'yx'},
              {'id':30, 'dir_name':'zx'} ]

app = Flask(__name__)
es = Elasticsearch(HOST='localhost', PORT=9200)


@app.route('/')
def index():
    return "Hello World!"


@app.route('/api')
def call_json():
    for i in list_json:
        create_folder(i)
        print("folder created")
        apply_elastic_search(i)
        print("wrote to db")
    return "it's done!"


def apply_elastic_search(item):
    """
    Store data from json in elastic search index

    Args:
    -----
    Item: Json item

    Returns:
    --------
    None or
    error message if some error has been occured
    """
    idx = 'cereb'
    try:
        es.indices.create(item['id'])
        print("index created")
        es.index(index=idx, doc_type=idx, body=item)
        print("db populated")
    except:
        return "some error happened"


def create_folder(item):
    """
    create a folder in local system

    Args:
    -----
    Item : A dict, which is converted from json
    
    Returns:
    --------
    None 
    error message, if some error occured
    """
    try:
        name = item['dir_name']
        raw_path = r"C:\Users\satya\Desktop"
        path_ = os.path.join(raw_path, name)
        os.mkdir(path_)
    except:
        return "some error occured"

if __name__ == '__main__':
    app.run(debug=True)
